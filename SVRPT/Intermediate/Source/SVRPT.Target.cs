using UnrealBuildTool;

public class SVRPTTarget : TargetRules
{
	public SVRPTTarget(TargetInfo Target) : base(Target)
	{
		DefaultBuildSettings = BuildSettingsVersion.V2;
		Type = TargetType.Game;
		ExtraModuleNames.Add("SVRPT");
	}
}
